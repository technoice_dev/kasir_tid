-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 08 Mar 2020 pada 05.21
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kasir`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `kd_barang` varchar(50) NOT NULL,
  `n_barang` varchar(50) NOT NULL,
  `harga` int(20) NOT NULL,
  `kuantitas` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`kd_barang`, `n_barang`, `harga`, `kuantitas`) VALUES
('488923', 'Nasi Uduk', 40000, 89329);

-- --------------------------------------------------------

--
-- Struktur dari tabel `brg_tr`
--

CREATE TABLE `brg_tr` (
  `id` int(11) NOT NULL,
  `id_transaksi` varchar(50) NOT NULL,
  `kd_barang` int(50) NOT NULL,
  `jumlah` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` varchar(20) NOT NULL,
  `n_pgw` varchar(50) NOT NULL,
  `full_alamat` text NOT NULL,
  `kota` varchar(20) NOT NULL,
  `provinsi` varchar(20) NOT NULL,
  `jenkel` varchar(10) NOT NULL,
  `jabatan` varchar(29) NOT NULL,
  `no_telp` int(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` varchar(50) NOT NULL,
  `tr_date` date NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tr_date`, `status`) VALUES
('20200215_1', '2020-02-15', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `gambar` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `gambar`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(4, 'anu', 'rizkyfajar_anugrah@yahoo.com', 'default.jpg', '$2y$10$FlLN/OhscLLAgWbWo1IVXOLtL6QVUGCZJx2zVuFVJmkzRygH7Wt3C', 2, 1, 1576995076),
(5, 'Rizky Fajar Huwaaa', 'rizky.fajar.anugrah@gmail.com', 'WIN_20160326_11_52_41_Pro.jpg', '$2y$10$IrDf141ZsyB2CnsB0v7Dk.LQ.AsdBWq/cB4ymxC.VH1AosOyWrSIW', 1, 1, 1577013041),
(6, 'rizkyfjra', 'rizkyfajaranugrah2@gmail.com', 'default.jpg', '$2y$10$IEAnxy8jae6zaaF.fnGet.rY0AO21plyDdOsH3eRwpCp0dq8woYCq', 2, 1, 1579358255),
(8, 'Dicky Nursalim', 'blackdesirewx9@gmail.com', 'default.jpg', '$2y$10$LW9OtLClZHIMHf8XNuCST.h/OQvZ5qA/7doSXIj6ioxvWOTxfbpa2', 1, 1, 1579698652);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 3, 2),
(5, 3, 2),
(7, 1, 5),
(9, 1, 9),
(10, 2, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(5, 'Menu'),
(9, 'Tentang App/Cara Penggunaan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Kelola Toko / Cafe', 'HalamanUtama', 'fas fa-fw fa-tachometer-alt', 1),
(2, 2, 'Profile', 'User', 'fas fa-fw fa-user-alt', 1),
(5, 1, 'Transaksi', 'transaksi', 'fas fa-fw fa-dollar-sign', 1),
(6, 5, 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1),
(7, 5, 'SubMenu Management', 'menu/submenu', 'far fa-fw fa-folder-open', 1),
(12, 1, 'Role', 'HalamanUtama/role', 'fas fa-fw fa-user-tie', 1),
(13, 2, 'Ubah Profile', 'user/edit', 'fas fa-fw fa-user-tie', 1),
(14, 2, 'Ganti Password', 'user/editpassword', 'fas fa-fw fa-key', 1),
(15, 9, 'Cara Penggunaan', 'carapakai', 'fas fa-fw fa-info', 1),
(16, 9, 'Fitur Kasir', 'carapakai/fitur', 'fas fa-fw fa-award', 1),
(17, 1, 'Stok Barang', 'dbarang', 'fas fa-fw fa-archive', 1),
(18, 1, 'List Transaksi', 'l_transaksi', 'fas fa-fw fa-folder', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `data_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_token`
--

INSERT INTO `user_token` (`id`, `email`, `token`, `data_created`) VALUES
(1, 'rizkyfajaranugrah2@gmail.com', 'S14Zv6zlq+MxoLnodPwr5Zm473kWZzKpYCxs2eiGeQc=', 0),
(2, 'rizky.fajar.anugrah@gmail.com', 'VJUL1IkfQn8aEMbyAkL2WWGBkrzwmAsONIk0NWruq30=', 0),
(3, 'rizky.fajar.anugrah@gmail.com', '7gr7GnbZPikUDuETPXmMJP3HWbsdeeZC3uPWdVtp5JA=', 0),
(4, 'rizkyfajaranugrah2@gmail.com', 'Zp6hnUPHT6eaYmqiBJFV0wE2F/VhX5A3K625c6vYBw0=', 0),
(5, 'rizkyfajaranugrah2@gmail.com', 'X4KH2wpfvJpxGrzQJposP2/QF+AagrjsHhXgGmMFnfk=', 0),
(6, 'rizkyfajaranugrah2@gmail.com', 'xMg63k/TxJ8R2sbO0/+pZyk3hCO4MIIFa1MRE1Ziltk=', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kd_barang`);

--
-- Indeks untuk tabel `brg_tr`
--
ALTER TABLE `brg_tr`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `brg_tr`
--
ALTER TABLE `brg_tr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

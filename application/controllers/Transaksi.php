<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{

	public function __construct()
	{
        
		parent::__construct();
		if (!$this->session->userdata('email')) {
			redirect('auth');
		}
        $this->load->model('transaksi_m');
        $this->load->model('barang');
        
		//cek_login();
	}

	public function index()
	{
        
        
		$data['title'] = 'Transaksi';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['no_transaksi'] = $this->transaksi_m->add()->id_transaksi;
        $data['barang'] = $this->barang->all_barang();
		$this->load->view('templates/header', $data);
		$this->load->view('transaksi');
		$this->load->view('templates/footer', $data);
	}
    public function cek()
    {
        $hasil = $this->transaksi_m->cek_query();
        echo json_encode($hasil);
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class L_transaksi extends CI_Controller
{
    
    public function index()
    {
     $data['title'] = 'Stok Barang';
        
        
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('admin/l_transaksi', $data);
        $this->load->view('templates/footer', $data);   
    }
    
}
?>